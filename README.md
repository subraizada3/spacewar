## Spacewar!
A small game I created based off of the original ([Wikipedia](https://en.wikipedia.org/wiki/Spacewar!)).

### Instructions
Download the [JAR](spacewar.jar).

`java -jar spacewar.jar` to run.

The screen sometimes flickers upon starting the game, exiting it and restarting fixes this issue.

Controls: WASD/space, arrow keys/zero \[numpad or top row]

### About
The sources were lost at some point, but I had a .jar which was decompiled to recreate the original code.
However, some constants were lost in the decompilation - for example, places where I wrote Input.Keys.<key\> now only contain the number which <key\> represented.

Built using [libGDX](https://libgdx.badlogicgames.com/).

The scrolling starfield in the background was based off of real-life stars in the original game. I found an image of it online, but can't find now where it came from.


### Screenshots
![](screenshots/screenshot1.png)

Thrusting releases particles. Bullets move fast enough that motion blur leaves the trail of dots, instead of blurring them.

![](screenshots/screenshot2.png)

Motion blur visible.

![](screenshots/screenshot3.png)

The explosions look cooler in the actual game.
Also showing win/loss indicators (near fuel bars at the bottom), to help when it's too close to tell.
