/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar.desktop;

import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.subraizada.spacewar.Spacewar;

public class DesktopLauncher {
	private static final boolean FULLSCREEN = true;

	public DesktopLauncher() {
	}

	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		DisplayMode d = LwjglApplicationConfiguration.getDesktopDisplayMode();
		config.setFromDisplayMode(d);
		config.vSyncEnabled = true;
		config.foregroundFPS = 60;
		config.resizable = false;
		new LwjglApplication(new Spacewar(), config);
	}
}
