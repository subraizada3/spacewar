/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import java.util.Iterator;

public class GameRenderer {
	private final GameWorld world;
	private final OrthographicCamera camera;
	private final Viewport viewport;
	private final ShapeRenderer sr;
	private final SpriteBatch sb;
	private final Texture backgroundTexture;
	private final Texture fade;
	private final Texture particle;
	private int currentBGLoc;
	private float bgUpdateTime;
	private int bgScrollDir;

	GameRenderer(GameWorld world) {
		this.world = world;
		this.camera = new OrthographicCamera();
		this.camera.setToOrtho(false, 512.0F, 512.0F);
		this.viewport = new FitViewport(512.0F, 512.0F, this.camera);
		this.sr = new ShapeRenderer();
		this.sr.setProjectionMatrix(this.camera.combined);
		this.sb = new SpriteBatch();
		this.sb.setProjectionMatrix(this.camera.combined);
		this.backgroundTexture = new Texture(Gdx.files.internal("background.png"));
		this.resetBackground();
		this.fade = new Texture(Gdx.files.internal("fade.png"));
		this.particle = new Texture(Gdx.files.internal("particle.png"));
	}

	public void render(float dt) {
		this.bgUpdateTime += dt;
		if (this.bgUpdateTime >= 0.1F) {
			this.bgUpdateTime = 0.0F;
			this.currentBGLoc += this.bgScrollDir;
			if (this.currentBGLoc == 4095) {
				this.currentBGLoc = 0;
			} else if (this.currentBGLoc == 0) {
				this.currentBGLoc = 4095;
			}
		}

		this.camera.update();
		this.sb.begin();
		this.sb.setColor(1.0F, 1.0F, 1.0F, 1.0F);
		this.sb.draw(this.fade, 0.0F, 0.0F);
		this.sb.draw(this.fade, 0.0F, 0.0F);
		this.sb.draw(this.backgroundTexture, 0.0F, 0.0F, this.currentBGLoc, 0, 512, 512);
		Iterator var2 = this.world.thrustParticles.iterator();

		while (var2.hasNext()) {
			ThrustParticle t = (ThrustParticle) var2.next();
			this.sb.setColor(t.r, t.g, t.b, 1.0F);
			this.sb.draw(this.particle, t.x - 1.5F * t.size, t.y - 1.5F * t.size, 0.0F, 0.0F, 3.0F, 3.0F, t.size, t.size, 0.0F, 0, 0, 3, 3, false, false);
		}

		this.sb.end();
		this.sr.begin(ShapeType.Filled);
		this.sr.setColor(0.4862745F, 0.9843137F, 0.8666667F, 1.0F);
		this.sr.rectLine(this.world.sunLine[0], this.world.sunLine[1], this.world.sunLine[2], this.world.sunLine[3], 1.2F);
		this.sr.setColor(1.0F, 1.0F, 1.0F, 1.0F);
		float[] f;
		if (!this.world.needle.isDead) {
			var2 = this.world.needle.triangles.iterator();

			while (var2.hasNext()) {
				f = (float[]) var2.next();
				this.sr.triangle(f[0], f[1], f[2], f[3], f[4], f[5]);
			}
		}

		if (!this.world.wedge.isDead) {
			var2 = this.world.wedge.triangles.iterator();

			while (var2.hasNext()) {
				f = (float[]) var2.next();
				this.sr.triangle(f[0], f[1], f[2], f[3], f[4], f[5]);
			}
		}

		this.sr.setColor(1.0F, 1.0F, 1.0F, 1.0F);
		var2 = this.world.torpedoes.iterator();

		while (var2.hasNext()) {
			Torpedo t = (Torpedo) var2.next();
			this.sr.circle(t.x, t.y, 1.0F);
		}

		this.sr.setColor(0.5F, 0.5F, 0.5F, 1.0F);
		this.sr.rect(0.0F, 0.0F, 100.0F, 4.0F);
		this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
		this.sr.rect(0.0F, 0.0F, this.world.needle.fuel, 4.0F);
		this.sr.setColor(0.5F, 0.5F, 0.5F, 1.0F);
		this.sr.rect(412.0F, 0.0F, 100.0F, 4.0F);
		this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
		this.sr.rect(512.0F - this.world.wedge.fuel, 0.0F, this.world.wedge.fuel, 4.0F);
		this.sr.setColor(0.0F, 1.0F, 0.0F, 1.0F);
		switch (this.world.deadShip) {
			case -1:
			default:
				break;
			case 0:
				this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
				this.sr.circle(6.0F, 10.0F, 5.0F);
				this.sr.setColor(0.0F, 1.0F, 0.0F, 1.0F);
				this.sr.circle(506.0F, 10.0F, 5.0F);
				break;
			case 1:
				this.sr.setColor(0.0F, 1.0F, 0.0F, 1.0F);
				this.sr.circle(6.0F, 10.0F, 5.0F);
				this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
				this.sr.circle(506.0F, 10.0F, 5.0F);
				break;
			case 2:
				this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
				this.sr.circle(6.0F, 10.0F, 5.0F);
				this.sr.setColor(1.0F, 0.0F, 0.0F, 1.0F);
				this.sr.circle(506.0F, 10.0F, 5.0F);
		}

		this.sr.end();
	}

	public void dispose() {
		this.sr.dispose();
		this.sb.dispose();
	}

	public void resize(int width, int height) {
		Gdx.gl.glClearColor(0.1F, 0.1F, 0.1F, 1.0F);
		Gdx.gl.glClear(16384);
		this.viewport.update(width, height, true);
		this.camera.update();
		this.sb.setProjectionMatrix(this.camera.combined);
		this.sr.setProjectionMatrix(this.camera.combined);
	}

	public void resetBackground() {
		this.currentBGLoc = MathUtils.random(0, 4094);
		this.bgScrollDir = MathUtils.random(0, 1);
		if (this.bgScrollDir == 0) {
			this.bgScrollDir = -1;
		}

	}
}
