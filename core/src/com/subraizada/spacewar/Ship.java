/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

public class Ship {
	public static final int TYPE_NEEDLE = 0;
	public static final int TYPE_WEDGE = 1;
	public static final int TYPE_NONE = -1;
	public static final int TYPE_BOTH = 2;
	private final int type;
	private static final float MAXFUEL = 100.0F;
	private static final float FUELDRAINRATE = 50.0F;
	private static final float FUELREFILLRATE = 0.2F;
	private static final float MINFUELTOTHRUST = 15.0F;
	private static final float TPDURATION = 2.0F;
	private static final float FIRERATE = 1.2F;
	private static final float THRUSTERPOWER = 20.0F;
	private static final float GRAVITYPOWER = 90000.0F;
	private static final float FRICTION = 0.05F;
	public static final float FUELCAPACITY = 100.0F;
	private static final float TURNRATE = 3.1415927F;
	public static final float RADIUS = 10.0F;
	public int PARTICLES_PER_SECOND;
	private final GameWorld world;
	public float x;
	public float y;
	public float dx;
	public float dy;
	public float angle;
	public float speed;
	public ArrayList<float[]> triangles;
	public ArrayList<float[]> lines;
	public float fuel;
	public float tpCountDown = 2.0F;
	public float gunHeat;
	private int tpTimes;
	public boolean inputTurningCCW;
	public boolean inputTurningCW;
	public boolean inputThrusting;
	public boolean inputFiring;
	public boolean inputTp;
	public boolean inTp;
	public boolean isThrusting;
	public boolean isDead;

	public Ship(float x, float y, float angle, int type, GameWorld world) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.world = world;
		this.fuel = 100.0F;
		this.type = type;
		this.triangles = new ArrayList();
		this.lines = new ArrayList();
		this.updateTriangles();
	}

	public void update(float dt) {
		if (!this.isDead) {
			if (this.inTp) {
				this.tpCountDown -= dt;
				if (this.tpCountDown <= 0.0F) {
					this.exitTp();
				}
			} else {
				this.fuel += 10.0F * dt;
				if (this.fuel > 100.0F) {
					this.fuel = 100.0F;
				}

				this.gunHeat -= dt;
				this.handleInput(dt);
				this.gravity(dt);
				this.friction(dt);
				this.x += this.dx * dt;
				this.y += this.dy * dt;
				this.speed = (float) Math.sqrt((double) (this.dx * this.dx + this.dy * this.dy));
				this.PARTICLES_PER_SECOND = 400;
				this.wrap();
				this.updateTriangles();
				this.updateLines();
			}
		}

	}

	private void updateTriangles() {
		this.triangles.clear();
		switch (this.type) {
			case 0:
				float[] pointsNeedle1 = new float[]{this.x + MathUtils.cos(this.angle + 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle + 0.11990398F) * 8.41962F, this.x + MathUtils.cos(this.angle + 2.6098685F) * 2.2113345F, this.y + MathUtils.sin(this.angle + 2.6098685F) * 2.2113345F, this.x + MathUtils.cos(this.angle - 2.6098685F) * 2.2113345F, this.y + MathUtils.sin(this.angle - 2.6098685F) * 2.2113345F};
				float[] pointsNeedle2 = new float[]{this.x + MathUtils.cos(this.angle - 2.6098685F) * 2.2113345F, this.y + MathUtils.sin(this.angle - 2.6098685F) * 2.2113345F, this.x + MathUtils.cos(this.angle - 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle - 0.11990398F) * 8.41962F, this.x + MathUtils.cos(this.angle + 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle + 0.11990398F) * 8.41962F};
				float[] pointsNeedle3 = new float[]{this.x + MathUtils.cos(this.angle - 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle - 2.0863452F) * 3.4481878F, this.x + MathUtils.cos(this.angle + 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle + 2.7700744F) * 8.263777F, this.x + MathUtils.cos(this.angle - 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle - 2.7700744F) * 8.263777F};
				float[] pointsNeedle4 = new float[]{this.x + MathUtils.cos(this.angle + 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle + 2.7700744F) * 8.263777F, this.x + MathUtils.cos(this.angle + 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle + 2.0863452F) * 3.4481878F, this.x + MathUtils.cos(this.angle - 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle - 2.0863452F) * 3.4481878F};
				this.triangles.add(pointsNeedle1);
				this.triangles.add(pointsNeedle2);
				this.triangles.add(pointsNeedle3);
				this.triangles.add(pointsNeedle4);
				break;
			case 1:
				float[] pointsWedge = new float[]{this.x + 8.0F * MathUtils.cos(this.angle), this.y + 8.0F * MathUtils.sin(this.angle), this.x + 8.0F * MathUtils.cos(this.angle + 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle + 2.6179938F), this.x + 8.0F * MathUtils.cos(this.angle - 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle - 2.6179938F)};
				this.triangles.add(pointsWedge);
		}

	}

	private void updateLines() {
		this.lines.clear();
		switch (this.type) {
			case 0:
				float[] nLine1 = new float[]{this.x + MathUtils.cos(this.angle + 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle + 0.11990398F) * 8.41962F, this.x + MathUtils.cos(this.angle - 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle - 0.11990398F) * 8.41962F};
				float[] nLine2 = new float[]{this.x + MathUtils.cos(this.angle + 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle + 0.11990398F) * 8.41962F, this.x + MathUtils.cos(this.angle + 2.6098685F) * 2.2113345F, this.y + MathUtils.sin(this.angle + 2.6098685F) * 2.2113345F};
				float[] nLine3 = new float[]{this.x + MathUtils.cos(this.angle - 0.11990398F) * 8.41962F, this.y + MathUtils.sin(this.angle - 0.11990398F) * 8.41962F, this.x + MathUtils.cos(this.angle - 2.6098685F) * 2.2113345F, this.y + MathUtils.sin(this.angle - 2.6098685F) * 2.2113345F};
				float[] nLine4 = new float[]{this.x + MathUtils.cos(this.angle + 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle + 2.0863452F) * 3.4481878F, this.x + MathUtils.cos(this.angle - 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle - 2.0863452F) * 3.4481878F};
				float[] nLine5 = new float[]{this.x + MathUtils.cos(this.angle + 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle + 2.0863452F) * 3.4481878F, this.x + MathUtils.cos(this.angle + 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle + 2.7700744F) * 8.263777F};
				float[] nLine6 = new float[]{this.x + MathUtils.cos(this.angle - 2.0863452F) * 3.4481878F, this.y + MathUtils.sin(this.angle - 2.0863452F) * 3.4481878F, this.x + MathUtils.cos(this.angle - 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle - 2.7700744F) * 8.263777F};
				float[] nLine7 = new float[]{this.x + MathUtils.cos(this.angle + 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle + 2.7700744F) * 8.263777F, this.x + MathUtils.cos(this.angle - 2.7700744F) * 8.263777F, this.y + MathUtils.sin(this.angle - 2.7700744F) * 8.263777F};
				this.lines.add(nLine1);
				this.lines.add(nLine2);
				this.lines.add(nLine3);
				this.lines.add(nLine4);
				this.lines.add(nLine5);
				this.lines.add(nLine6);
				this.lines.add(nLine7);
				break;
			case 1:
				float[] line1 = new float[]{this.x + 8.0F * MathUtils.cos(this.angle), this.y + 8.0F * MathUtils.sin(this.angle), this.x + 8.0F * MathUtils.cos(this.angle + 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle + 2.6179938F)};
				float[] line2 = new float[]{this.x + 8.0F * MathUtils.cos(this.angle + 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle + 2.6179938F), this.x + 8.0F * MathUtils.cos(this.angle - 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle - 2.6179938F)};
				float[] line3 = new float[]{this.x + 8.0F * MathUtils.cos(this.angle - 2.6179938F), this.y + 8.0F * MathUtils.sin(this.angle - 2.6179938F), this.x + 8.0F * MathUtils.cos(this.angle), this.y + 8.0F * MathUtils.sin(this.angle)};
				this.lines.add(line1);
				this.lines.add(line2);
				this.lines.add(line3);
		}

	}

	private void handleInput(float dt) {
		if (this.inputTp && !this.inTp) {
			this.beginTp();
			this.inputTp = false;
		}

		if (this.inputTurningCCW) {
			this.angle += 3.1415927F * dt;
			this.inputTurningCCW = false;
		}

		if (this.inputTurningCW) {
			this.angle -= 3.1415927F * dt;
			this.inputTurningCW = false;
		}

		if (this.inputThrusting && this.isThrusting && this.fuel >= 50.0F * dt) {
			this.thrust(dt);
			this.inputThrusting = false;
		} else if (this.inputThrusting && this.fuel >= 15.0F) {
			this.thrust(dt);
			this.inputThrusting = false;
		} else {
			this.isThrusting = false;
			this.inputThrusting = false;
		}

		if (this.inputFiring && !this.inTp) {
			if (this.gunHeat <= 0.0F) {
				this.fire();
			}

			this.inputFiring = false;
		}

	}

	private void fire() {
		this.gunHeat = 1.2F;
		this.world.addTorpedo(new Torpedo(this.x + 10.0F * MathUtils.cos(this.angle), this.y + 10.0F * MathUtils.sin(this.angle), this.angle, this.speed));
	}

	private void beginTp() {
		int chanceDead;
		if (this.tpTimes == 0) {
			chanceDead = 0;
		} else {
			chanceDead = 30 + (this.tpTimes + 1 ^ 2);
		}

		if (MathUtils.random(100) < chanceDead) {
			this.die();
		} else {
			++this.tpTimes;
			this.inTp = true;
			if (this.type == 0) {
				this.x = 1000000.0F;
				this.y = 1000000.0F;
			} else {
				this.x = -1000000.0F;
				this.y = -1000000.0F;
			}

		}
	}

	private void exitTp() {
		this.inTp = false;
		this.tpCountDown = 2.0F;
		this.x = (float) MathUtils.random(0, 511);
		this.y = (float) MathUtils.random(0, 511);
	}

	private void wrap() {
		if (this.x > 512.0F) {
			this.x -= 512.0F;
		}

		if (this.y > 512.0F) {
			this.y -= 512.0F;
		}

		if (this.x < 0.0F) {
			this.x = 512.0F - this.x;
		}

		if (this.y < 0.0F) {
			this.y = 512.0F - this.y;
		}

	}

	public void die() {
		if (!this.isDead) {
			this.isDead = true;
			this.world.shipDied(this.type);

			for (int i = 0; i < MathUtils.random(50, 150); ++i) {
				this.world.thrustParticles.add(new ThrustParticle(this.x, this.y, MathUtils.random(6.2831855F)));
			}

		}
	}

	private void friction(float dt) {
		float speed = this.world.distance(this.dx, this.dy, 0.0F, 0.0F);
		float friction = speed * 0.05F;
		float moveAngle = MathUtils.atan2(this.dy, this.dx);
		speed -= friction * dt;
		this.dx = speed * MathUtils.cos(moveAngle);
		this.dy = speed * MathUtils.sin(moveAngle);
	}

	private void thrust(float dt) {
		this.isThrusting = true;
		this.dx += 20.0F * MathUtils.cos(this.angle) * dt;
		this.dy += 20.0F * MathUtils.sin(this.angle) * dt;
		this.fuel -= 50.0F * dt;
		int numParticlesToSpawn = (int) ((float) this.PARTICLES_PER_SECOND * dt);
		float spawnAngleVariation = 0.017453292F * (float) Math.sqrt((double) this.speed) * 1.8F;
		float spawnAngle = this.angle + MathUtils.random(-spawnAngleVariation, spawnAngleVariation);

		for (int i = 0; i < numParticlesToSpawn; ++i) {
			this.world.thrustParticles.add(new ThrustParticle(this.x - 8.0F * MathUtils.cos(spawnAngle), this.y - 8.0F * MathUtils.sin(spawnAngle), this.angle - (spawnAngle - this.angle) + 3.1415927F, 17.0F));
		}

	}

	private void gravity(float dt) {
		float distX = this.x - 256.0F;
		float distY = this.y - 256.0F;
		float dist = (float) Math.sqrt((double) (distX * distX + distY * distY));
		float dist2 = (float) Math.pow((double) dist, 2.0D);
		float angle = MathUtils.atan2(distY, distX);
		this.dx -= dt * 90000.0F * MathUtils.cos(angle) / dist2;
		this.dy -= dt * 90000.0F * MathUtils.sin(angle) / dist2;
	}
}
