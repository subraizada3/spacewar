/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.math.MathUtils;

public class ThrustParticle {
	public static final float angleVariation = 0.08726646F;
	public float x;
	public float y;
	public float dx;
	public float dy;
	public float r = 1.0F;
	public float g = 0.0F;
	public float b = 0.0F;
	public float size;
	public float lifespan;
	public float lifetime;
	public float dRed;
	public float dGreen;
	public float dBlue;
	public float dSize;

	public ThrustParticle(float x, float y, float angle, float speed) {
		this.x = x;
		this.y = y;
		angle += MathUtils.random(-0.08726646F, 0.08726646F);
		this.dx = speed * MathUtils.cos(angle);
		this.dy = speed * MathUtils.sin(angle);
		this.lifespan = MathUtils.random(0.1F, 0.2F);
		this.size = 1.5F;
		this.dRed = 1.0F / this.lifespan * -50.0F / 255.0F;
		this.dGreen = 1.0F / this.lifespan * 220.0F / 255.0F;
		this.dBlue = 1.0F / this.lifespan * 30.0F / 255.0F;
		this.dSize = 1.0F / this.lifespan * -0.6F;
	}

	public ThrustParticle(float x, float y, float angle) {
		this.x = x;
		this.y = y;
		float speed = 30.0F;
		this.dx = speed * MathUtils.cos(angle);
		this.dy = speed * MathUtils.sin(angle);
		this.lifespan = MathUtils.random(0.4F, 0.9F);
		this.size = 3.0F;
		switch (MathUtils.random(0, 4)) {
			case 0:
			case 1:
			case 2:
				this.r = 0.627451F;
				this.g = 0.54901963F;
				this.b = 0.11764706F;
				this.dRed = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dGreen = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dBlue = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dSize = 1.0F / this.lifespan * -1.5F;
				break;
			case 3:
				this.r = 0.23529412F;
				this.g = 0.11764706F;
				this.b = 0.039215688F;
				this.dRed = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dGreen = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dBlue = 1.0F / this.lifespan * 0.0F / 255.0F;
				this.dSize = 1.0F / this.lifespan * -1.5F;
				break;
			case 4:
				this.r = 0.74509805F;
				this.g = 0.74509805F;
				this.b = 0.74509805F;
				this.dRed = 1.0F / this.lifespan * -100.0F / 255.0F;
				this.dGreen = 1.0F / this.lifespan * -100.0F / 255.0F;
				this.dBlue = 1.0F / this.lifespan * -100.0F / 255.0F;
				this.dSize = 1.0F / this.lifespan * -1.5F;
		}

	}

	public void update(float dt) {
		this.lifetime += dt;
		this.x += this.dx * dt;
		this.y += this.dy * dt;
		this.size += this.dSize * dt;
		this.r += this.dRed * dt;
		this.g += this.dGreen * dt;
		this.b += this.dBlue * dt;
	}

	public boolean needsRemoval() {
		return this.lifetime >= this.lifespan;
	}
}
