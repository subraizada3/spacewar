/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;

public class GameWorld {
	public static final float MAX_SUN_SIZE = 7.5F;
	public Ship needle;
	public Ship wedge;
	public final ArrayList<Torpedo> torpedoes;
	public final ArrayList<ThrustParticle> thrustParticles;
	public float[] sunLine;
	public int deadShip = -1;
	public boolean paused = false;
	private float secondsSinceDeath = -1.0F;
	public boolean showTutorial;
	private Iterator<Torpedo> tIterator;
	private Iterator<ThrustParticle> thrustIterator;
	private Torpedo currentTorpedoBeingIterated;
	private ThrustParticle currentTPBeingIterated;
	private GameScreen screen;
	private GameRenderer renderer;
	private Preferences prefs;

	public GameWorld(GameScreen screen) {
		this.screen = screen;
		this.needle = new Ship(384.0F, 384.0F, -1.5707964F, 0, this);
		this.wedge = new Ship(128.0F, 128.0F, 1.5707964F, 1, this);
		this.torpedoes = new ArrayList();
		this.thrustParticles = new ArrayList(500);
		this.sunLine = new float[4];
		this.renderer = new GameRenderer(this);
		this.prefs = Gdx.app.getPreferences("Spacewar!");
		this.showTutorial = this.prefs.getBoolean("show tutorial", true);
	}

	public void update(float dt) {
		this.handleInput();
		if (!this.paused) {
			if (this.secondsSinceDeath >= 0.0F) {
				this.secondsSinceDeath += dt;
			}

			if (this.secondsSinceDeath >= 3.0F) {
				this.restart();
				this.secondsSinceDeath = -1.0F;
			}

			this.thrustIterator = this.thrustParticles.iterator();

			while (this.thrustIterator.hasNext()) {
				this.currentTPBeingIterated = (ThrustParticle) this.thrustIterator.next();
				this.currentTPBeingIterated.update(dt);
				if (this.currentTPBeingIterated.needsRemoval()) {
					this.thrustIterator.remove();
				}
			}

			this.updateSun();
			this.needle.update(dt);
			this.wedge.update(dt);
			this.tIterator = this.torpedoes.iterator();

			while (this.tIterator.hasNext()) {
				this.currentTorpedoBeingIterated = (Torpedo) this.tIterator.next();
				this.currentTorpedoBeingIterated.update(dt);
				if (this.currentTorpedoBeingIterated.outOfBounds()) {
					this.tIterator.remove();
				}
			}

			this.collideSun();
			this.collideTorpedoes();
			this.collideShips();
			this.renderer.render(dt);
		}

	}

	private void updateSun() {
		float sunLength = MathUtils.random(7.5F);
		float sunAngle = MathUtils.random(6.2831855F);
		this.sunLine[0] = 256.0F - sunLength * MathUtils.cos(sunAngle);
		this.sunLine[1] = 256.0F - sunLength * MathUtils.sin(sunAngle);
		this.sunLine[2] = 256.0F + sunLength * MathUtils.cos(sunAngle);
		this.sunLine[3] = 256.0F + sunLength * MathUtils.sin(sunAngle);
	}

	public void collideSun() {
		Iterator var1;
		float[] f;
		if (this.distance(this.needle.x, this.needle.y, 256.0F, 256.0F) < 22.5F) {
			var1 = this.needle.lines.iterator();

			while (var1.hasNext()) {
				f = (float[]) var1.next();
				if (this.lineIntersects(f, this.sunLine)) {
					this.needle.die();
					break;
				}
			}
		}

		if (this.distance(this.wedge.x, this.wedge.y, 256.0F, 256.0F) < 22.5F) {
			var1 = this.wedge.lines.iterator();

			while (var1.hasNext()) {
				f = (float[]) var1.next();
				if (this.lineIntersects(f, this.sunLine)) {
					this.wedge.die();
					break;
				}
			}
		}

	}

	public void collideTorpedoes() {
		Iterator var1 = this.torpedoes.iterator();

		while (true) {
			while (true) {
				Torpedo t;
				Iterator var3;
				float[] f;
				do {
					if (!var1.hasNext()) {
						return;
					}

					t = (Torpedo) var1.next();
					if (this.distance(this.needle.x, this.needle.y, t.x, t.y) < 0.1F * this.distance(t.dx, t.dy, 0.0F, 0.0F)) {
						var3 = this.needle.lines.iterator();

						while (var3.hasNext()) {
							f = (float[]) var3.next();
							if (this.lineIntersects(f, t.line)) {
								this.needle.die();
								break;
							}
						}
					}
				}
				while (this.distance(this.wedge.x, this.wedge.y, t.x, t.y) >= 0.1F * this.distance(t.dx, t.dy, 0.0F, 0.0F));

				var3 = this.wedge.lines.iterator();

				while (var3.hasNext()) {
					f = (float[]) var3.next();
					if (this.lineIntersects(f, t.line)) {
						this.wedge.die();
						break;
					}
				}
			}
		}
	}

	private void collideShips() {
		Iterator var1 = this.needle.lines.iterator();

		while (var1.hasNext()) {
			float[] n = (float[]) var1.next();
			Iterator var3 = this.wedge.lines.iterator();

			while (var3.hasNext()) {
				float[] w = (float[]) var3.next();
				if (this.lineIntersects(n, w)) {
					this.bothDied();
				}
			}
		}

	}

	private void bothDied() {
		this.shipDied(2);
		this.wedge.die();
		this.needle.die();
	}

	public void addTorpedo(Torpedo t) {
		this.torpedoes.add(t);
	}

	private void handleInput() {
		if (Gdx.input.isKeyJustPressed(44)) {
			this.paused = !this.paused;
		} else {
			if (Gdx.input.isKeyPressed(29)) {
				this.needle.inputTurningCCW = true;
			}

			if (Gdx.input.isKeyPressed(32)) {
				this.needle.inputTurningCW = true;
			}

			if (Gdx.input.isKeyPressed(51)) {
				this.needle.inputFiring = true;
			}

			if (Gdx.input.isKeyPressed(47)) {
				this.needle.inputThrusting = true;
			}

			if (Gdx.input.isKeyJustPressed(62) && !this.needle.inTp) {
				this.needle.inputTp = true;
			}

			if (Gdx.input.isKeyPressed(21)) {
				this.wedge.inputTurningCCW = true;
			}

			if (Gdx.input.isKeyPressed(22)) {
				this.wedge.inputTurningCW = true;
			}

			if (Gdx.input.isKeyPressed(19)) {
				this.wedge.inputFiring = true;
			}

			if (Gdx.input.isKeyPressed(20)) {
				this.wedge.inputThrusting = true;
			}

			if (Gdx.input.isKeyJustPressed(7) || Gdx.input.isKeyJustPressed(144) && !this.wedge.inTp) {
				this.wedge.inputTp = true;
			}

			if (Gdx.input.isKeyJustPressed(46)) {
				this.restart();
			}

			if (Gdx.input.isKeyJustPressed(48)) {
				this.showTutorial = !this.showTutorial;
				this.prefs.putBoolean("show tutorial", this.showTutorial);
				this.prefs.flush();
			}

			if (Gdx.input.isKeyPressed(67)) {
				Gdx.app.exit();
			}

		}
	}

	public void shipDied(int type) {
		if (this.deadShip == -1) {
			this.deadShip = type;
			this.secondsSinceDeath = 0.1F;
		}

	}

	public void restart() {
		this.deadShip = -1;
		this.secondsSinceDeath = -1.0F;
		this.needle = new Ship(384.0F, 384.0F, -1.5707964F, 0, this);
		this.wedge = new Ship(128.0F, 128.0F, 1.5707964F, 1, this);
		this.torpedoes.clear();
		this.thrustParticles.clear();
		this.renderer.resetBackground();
	}

	public void resize(int width, int height) {
		this.renderer.resize(width, height);
	}

	public void dispose() {
		this.renderer.dispose();
	}

	public boolean lineIntersects(float[] l1, float[] l2) {
		return Intersector.intersectSegments(l1[0], l1[1], l1[2], l1[3], l2[0], l2[1], l2[2], l2[3], (Vector2) null);
	}

	public float distance(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt(Math.pow((double) (x1 - x2), 2.0D) + Math.pow((double) (y1 - y2), 2.0D));
	}
}
