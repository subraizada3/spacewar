/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.Screen;

public class GameScreen implements Screen {
	private final Spacewar game;
	private final GameWorld world;

	public GameScreen(Spacewar game) {
		this.game = game;
		this.world = new GameWorld(this);
	}

	@Override
	public void render(float dt) {
		this.world.update(dt);
	}

	@Override
	public void resize(int width, int height) {
		this.world.resize(width, height);
	}

	@Override
	public void dispose() {
		this.world.dispose();
	}

	@Override
	public void show() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}
}
