/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class MainMenuScreen implements Screen {
	private final Spacewar game;
	private final OrthographicCamera camera;
	private final Viewport viewport;
	private final ShapeRenderer sr;
	private final SpriteBatch sb;
	private final Texture background;
	private final Texture fade;
	private int currentBGLoc;
	private float bgUpdateTime;
	private int bgScrollDir;
	private float[] sunLine = new float[4];
	private final BitmapFont bigFont;
	private final BitmapFont smallFont;

	public MainMenuScreen(Spacewar game) {
		this.game = game;
		this.camera = new OrthographicCamera();
		this.camera.setToOrtho(false, 512.0F, 512.0F);
		this.viewport = new FitViewport(512.0F, 512.0F, this.camera);
		this.sr = new ShapeRenderer();
		this.sr.setProjectionMatrix(this.camera.combined);
		this.sb = new SpriteBatch();
		this.sb.setProjectionMatrix(this.camera.combined);
		this.background = new Texture(Gdx.files.internal("background.png"));
		this.fade = new Texture(Gdx.files.internal("fade.png"));
		this.currentBGLoc = MathUtils.random(0, 4094);
		this.bgScrollDir = MathUtils.random(0, 1);
		if (this.bgScrollDir == 0) {
			this.bgScrollDir = -1;
		}

		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("courier.ttf"));
		FreeTypeFontParameter param = new FreeTypeFontParameter();
		int screenHeight = Gdx.graphics.getHeight();
		param.genMipMaps = true;
		param.magFilter = TextureFilter.MipMapLinearLinear;
		param.minFilter = TextureFilter.Nearest;
		param.size = (int) (0.05D * (double) screenHeight);
		gen.scaleForPixelHeight(param.size);
		this.bigFont = gen.generateFont(param);
		this.bigFont.setColor(0.34117648F, 0.8784314F, 1.0F, 1.0F);
		param.size = (int) (0.02D * (double) screenHeight);
		gen.scaleForPixelHeight(param.size);
		this.smallFont = gen.generateFont(param);
		this.smallFont.setColor(0.34117648F, 0.8784314F, 1.0F, 1.0F);
		Gdx.gl.glClearColor(0.1F, 0.1F, 0.1F, 1.0F);
		Gdx.gl.glClear(16384);
	}

	@Override
	public void render(float dt) {
		this.camera.update();
		if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(66)) {
			this.game.setScreen(new GameScreen(this.game));
		}

		if (Gdx.input.isKeyPressed(67)) {
			Gdx.app.exit();
		}

		this.bgUpdateTime += dt;
		if (this.bgUpdateTime >= 0.1F) {
			this.bgUpdateTime = 0.0F;
			this.currentBGLoc += 1 * this.bgScrollDir;
			if (this.currentBGLoc == 4095) {
				this.currentBGLoc = 0;
			}
		}

		this.sb.begin();
		this.sr.setColor(1.0F, 1.0F, 1.0F, 1.0F);
		this.sb.draw(this.fade, 0.0F, 0.0F);
		this.sb.draw(this.fade, 0.0F, 0.0F);
		this.sb.draw(this.background, 0.0F, 0.0F, this.currentBGLoc, 0, 512, 512);
		this.bigFont.draw(this.sb, "SPACEWAR!", 0.0F, 400.0F, 512.0F, 1, false);
		this.smallFont.draw(this.sb, "Press Enter or click to begin", 0.0F, 180.0F, 512.0F, 1, false);
		this.smallFont.draw(this.sb, "Press Backspace to quit", 0.0F, 150.0F, 512.0F, 1, false);
		this.sb.end();
		float sunLength = MathUtils.random(7.5F);
		float sunAngle = MathUtils.random(6.2831855F);
		this.sunLine[0] = 256.0F - sunLength * MathUtils.cos(sunAngle);
		this.sunLine[1] = 256.0F - sunLength * MathUtils.sin(sunAngle);
		this.sunLine[2] = 256.0F + sunLength * MathUtils.cos(sunAngle);
		this.sunLine[3] = 256.0F + sunLength * MathUtils.sin(sunAngle);
		this.sr.begin(ShapeType.Filled);
		this.sr.setColor(0.4862745F, 0.9843137F, 0.8666667F, 1.0F);
		this.sr.rectLine(this.sunLine[0], this.sunLine[1], this.sunLine[2], this.sunLine[3], 1.2F);
		this.sr.end();
	}

	@Override
	public void resize(int width, int height) {
		Gdx.gl.glClearColor(0.1F, 0.1F, 0.1F, 1.0F);
		Gdx.gl.glClear(16384);
		this.viewport.update(width, height, true);
	}

	@Override
	public void dispose() {
		this.sr.dispose();
		this.sb.dispose();
	}

	@Override
	public void show() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}
}
