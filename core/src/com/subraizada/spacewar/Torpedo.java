/*******************************************************************************
 * Copyright 2017 Subramaniyam Raizada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.subraizada.spacewar;

import com.badlogic.gdx.math.MathUtils;

public class Torpedo {
	public static final float MOVESPEED = 250.0F;
	public static final float SIZE = 1.0F;
	public float x;
	public float y;
	public float dx;
	public float dy;
	public float angle;
	public float[] line = new float[4];

	public Torpedo(float x, float y, float angle, float speedBoost) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.dx = (250.0F + speedBoost) * MathUtils.cos(angle);
		this.dy = (250.0F + speedBoost) * MathUtils.sin(angle);
	}

	public void update(float dt) {
		float newX = this.x + this.dx * dt;
		float newY = this.y + this.dy * dt;
		this.line[0] = this.x;
		this.line[1] = this.y;
		this.line[2] = newX;
		this.line[3] = newY;
		this.x = newX;
		this.y = newY;
	}

	public boolean outOfBounds() {
		return this.x > 512.0F || this.y > 512.0F || this.x < 0.0F || this.y < 0.0F;
	}
}
